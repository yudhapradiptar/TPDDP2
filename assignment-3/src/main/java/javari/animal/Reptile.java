package javari.animal;

public class Reptile extends Animal{
    protected boolean isTame = false;

    public Reptile(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition, boolean isTame){
        super(id,type,name,gender,length,weight,condition);
        this.isTame = isTame;
    }

    @Override
    protected boolean specificCondition() {
        if(this.isTame == true){
            return true;
        }
        else {
            return false;
        }
    }
}
