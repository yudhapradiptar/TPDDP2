package javari.animal;

public class Aves extends Animal {
    protected boolean isLayingEggs = false;
    public Aves(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition, boolean isLayingEggs){
        super(id,type,name,gender,length,weight,condition);
        this.isLayingEggs = isLayingEggs;
    }
    @Override
    protected boolean specificCondition() {
        if (this.isLayingEggs == false){
            return true;
        }
        else {
            return false;
        }
    }
}
