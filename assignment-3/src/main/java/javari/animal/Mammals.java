package javari.animal;

public class Mammals extends Animal {
    protected boolean isPregnant = false;


    public Mammals(Integer id, String type, String name, Gender gender, double length, double weight, Condition condition, boolean isPregnant){
        super(id,type,name,gender,length,weight,condition);
        this.isPregnant = isPregnant;
    }

    protected  boolean specificCondition(){
        if (this.getType().equalsIgnoreCase("lion") == false) {
            if (this.isPregnant) {
                return false;
            } else {
                return true;
            }
        }
        else {
            if (this.getGender() == Gender.MALE){
                return true;
            }
            else {
                return false;
            }
        }
    }
}
