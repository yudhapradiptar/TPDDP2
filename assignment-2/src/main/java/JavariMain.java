import animal.*;
import cage.*;

import java.util.Scanner;
import java.util.ArrayList;

class JavariMain{
	private static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args){
		System.out.println("Welcome to Javari Park!" + 
			"\nInput the Number of Animals");
		System.out.print("cat: ");
		int inputJumlahCat = Integer.parseInt(input.nextLine());
		ToDoCage.createCage(Animals.create("cat", inputJumlahCat), "cat");
		System.out.print("lion: ");
		int inputJumlahLion = Integer.parseInt(input.nextLine());
		ToDoCage.createCage(Animals.create("lion", inputJumlahLion), "lion");
		System.out.print("eagle: ");
		int inputJumlahEagle = Integer.parseInt(input.nextLine());
		ToDoCage.createCage(Animals.create("eagle", inputJumlahEagle), "eagle");
		System.out.print("parrot: ");
		int inputJumlahParrot = Integer.parseInt(input.nextLine());
		ToDoCage.createCage(Animals.create("parrot", inputJumlahParrot), "parrot");
		System.out.print("hamster: ");
		int inputJumlahHamster = Integer.parseInt(input.nextLine());
		ToDoCage.createCage(Animals.create("hamster", inputJumlahHamster), "hamster");
		
		System.out.print("\nAnimals has been successfully recorded!"
		+ "\n============================================="
		+ "\nCage arrangement:");
		
		ToDoCage.printCage("cat");
		ToDoCage.printCage("lion");
		ToDoCage.printCage("eagle");
		ToDoCage.printCage("parrot");
		ToDoCage.printCage("hamster");
		
		System.out.println("\nNUMBER OF ANIMALS:");
		System.out.println("cat:" + inputJumlahCat);
		System.out.println("lion:" + inputJumlahLion);
		System.out.println("eagle:" + inputJumlahEagle);
		System.out.println("parrot:" + inputJumlahParrot);
		System.out.println("hamster:" + inputJumlahHamster);
		
		System.out.print("============================================= ");
		while(true){
			System.out.println("\nWhich animal you want to visit?\n" + 
			"(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
			String inputVisit = input.nextLine();
				if(inputVisit.equals("1")){
					ToDoCage.visitCage("cat");
				}
				else if(inputVisit.equals("2")){
					ToDoCage.visitCage("eagle");
				}
				else if(inputVisit.equals("3")){
					ToDoCage.visitCage("hamster");
				}
				else if(inputVisit.equals("4")){
					ToDoCage.visitCage("parrot");
				}
				else if(inputVisit.equals("5")){
					ToDoCage.visitCage("lion");
				}
				else if(inputVisit.equals("99")){
					System.exit(0);
					break;
				}
				else{
					System.out.println("Wrong command");
				}
		}
	}
}