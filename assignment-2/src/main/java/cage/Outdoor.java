package cage;
import animal.*;

class Outdoor extends Cage{

	public Outdoor(Animals animal){
		super(animal, "outdoor");
	}
	
	public static char cageType(int length){
		if(length < 75){
			return 'A';
		}
		else if(length <= 90){
			return 'B';
		}
		else{
			return 'C';
		}
	}
}