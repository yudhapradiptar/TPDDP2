package cage;
import animal.*;

import java.util.Scanner;

public class ToDoCage{
	private static Cage[][] cats;
	private static Cage[][] eagles;
	private static Cage[][] hamsters;
	private static Cage[][] parrots;
	private static Cage[][] lions;
	

	public static void printCage(String species){
		Cage[][] cage = checkCage(species);
		if(cage == null){
			return;
		}
		System.out.print("\nlocation: indoor");
		for(int i = 2; i >= 0; i--){
			System.out.print("\nlevel " + (i+1) + ": ");
			for(int j = 0; j < cage[i].length; j++){
				if(cage[i][j] == null){
					continue;
				}
				System.out.print(cage[i][j].getAnimal().getName() + " (" + cage[i][j].getAnimal().getLength() + " - " + cage[i][j].getType() + "), ");
			}
		}
		afterArrangeCage(cage);
	}
	
	public static void afterArrangeCage(Cage[][] cage){
		System.out.print("\n\nAfter rearrangement...");
		for(int i = 2; i > 0; i--){
			Cage[] temp = cage[i];
			cage[i] = cage[i-1];
			cage[i-1] = temp;
		}
		int count = 1;
		for(int i = 2; i >= 0; i--){
			while(count != cage[i].length-1){
				for(int j = 0; j < cage[i].length-count; j++){
					Cage temp = cage[i][j];
					cage[i][j] = cage[i][j+1];
					cage[i][j+1] = temp;
					count += 1;
				}
			}
			count = 0;
		}
		for(int i = 2; i >= 0; i--){
			System.out.print("\nlevel " + (i+1) + ": ");
			for(int j = 0; j < cage[i].length; j++){
				if(cage[i][j] == null){
					continue;
				}
				System.out.print(cage[i][j].getAnimal().getName() + " (" + cage[i][j].getAnimal().getLength() + " - " + cage[i][j].getType() + "), ");
			}
		}
		System.out.print("\n");
	}
	
	public static void createCage(Animals[] infoMasingHewan, String species){
		if(infoMasingHewan == null){
			return;
		}
		Cage[][] cage = checkCage(species, infoMasingHewan.length);
		int count = 0;
		for(int i = 0; i < 3; i++){
			if(infoMasingHewan.length <= count){
				return;
			}
			if(infoMasingHewan.length/3 == 0){
				if(species.equals("cat") || species.equals("parrot") || species.equals("hamster")){
					cage[i][0] = new Indoor(infoMasingHewan[count]);
				}
				else{
					cage[i][0] = new Outdoor(infoMasingHewan[count]);
				}
				count += 1;
			}
			else{
				for(int j = 0; j < infoMasingHewan.length/3; j++){
					if(i == 2 && infoMasingHewan.length % 3 > 0){
						for(int k = 1; k <= infoMasingHewan.length % 3; k++){
							if(species.equals("cat") || species.equals("parrot") || species.equals("hamster")){
								cage[i][j+k] = new Indoor(infoMasingHewan[count+k]);
							}
							else{
								cage[i][j+k] = new Outdoor(infoMasingHewan[count+k]);
							}
						}
					}
					if(species.equals("cat") || species.equals("parrot") || species.equals("hamster")){
						cage[i][j] = new Indoor(infoMasingHewan[count]);
					}
					else{
						cage[i][j] = new Outdoor(infoMasingHewan[count]);
					}
					count += 1;
				}
			}
		}
	}
	
			
	public static void visitCage(String species){
		Scanner input = new Scanner(System.in);
		System.out.print("Mention the name of " + species + " you want to visit: ");
		String name = input.nextLine();
		Cage[][] cage = checkCage(species);
		boolean found = false;
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < cage[i].length; j++){
				if(cage[i][j] == null || cage[i] == null){
					continue;
				}
				else if(cage[i][j].getAnimal().getName().equalsIgnoreCase(name)){
					found = true;
					System.out.println("You are visiting " + cage[i][j].getAnimal().getName()
					+ " (" + species + ") now," + "what would you like to do?");
					cage[i][j].getAnimal().visit(cage[i][j].getAnimal(), cage[i][j].getAnimal().getName(), species);
				}
			}
		}
		if(found == false){
			System.out.println("There is no " + species + " with that name! Back to the office!");
		}
	}
	
	public static Cage[][] checkCage(String species, int length){
		if(species.equals("cat")){
			cats = new Cage[3][length];
			return cats;
		}
		else if(species.equals("hamster")){
			hamsters = new Cage[3][length];
			return hamsters;
		}
		else if(species.equals("parrot")){
			parrots = new Cage[3][length];
			return parrots;
		}
		else if(species.equals("eagle")){
			eagles = new Cage[3][length];
			return eagles;
		}
		else{
			lions = new Cage[3][length];
			return lions;
		}
	}
	
	public static Cage[][] checkCage(String species){
		if(species.equals("cat")){
			return cats;
		}
		else if(species.equals("hamster")){
			return hamsters;
		}
		else if(species.equals("parrot")){
			return parrots;
		}
		else if(species.equals("eagle")){
			return eagles;
		}
		else{
			return lions;
		}
	}
}