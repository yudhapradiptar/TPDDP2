package cage;
import animal.*;

class Indoor extends Cage{
	
	public Indoor(Animals animal){
		super(animal, "indoor");
	}
	
	public static char cageType(int length){
		if(length < 45){
			return 'A';
		}
		else if(length <= 60){
			return 'B';
		}
		else{
			return 'C';
		}
	}
}