package animal;

import java.util.Scanner;

public class Eagles extends Animals{
	
	public Eagles(String name, int length){
		super(name, length, "wild");
	}


	public void flyHigh(){
		System.out.println(this.name + " makes a voice: Kwaakk....");
		System.out.println("You hurt!");
	}

	public void visit(String name){
		Scanner input = new Scanner(System.in);
		System.out.println("1: Order to Fly");
		String command = input.nextLine();
		if(command.equals("1")){
			this.flyHigh();
		}
		else{
			System.out.println("You do nothing...");
		}
	}
}