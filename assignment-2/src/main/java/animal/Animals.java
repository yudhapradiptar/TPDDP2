package animal;

import java.util.Scanner;

public class Animals{
	protected String name;
	protected int length;
	private String kind;
	
	public Animals(String name, int length, String kind){
		this.name = name;
		this.length = length;
		this.kind = kind;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getLength(){
		return this.length;
	}
	
	public String getKind(){
		return this.kind;
	}
	
	public static Animals[] create(String species, int jumlahHewan){
		Scanner input = new Scanner(System.in);
		if(jumlahHewan == 0){
			return null;
		}
		else{
			System.out.println("Provide the information of " + species + "(s):");
			String[] info = input.nextLine().split(",");
			Animals[] infoMasingHewan = new Animals[info.length];
			for(int i = 0; i < info.length; i++){
				String[] detail = info[i].split("\\|");	
					if(species.equals("cat")){ 
						infoMasingHewan[i] = new Cats(detail[0], Integer.parseInt(detail[1]));
					}
					else if(species.equals("eagle")){  
						infoMasingHewan[i] = new Eagles(detail[0], Integer.parseInt(detail[1]));
					}
					else if(species.equals("hamster")){ 
						infoMasingHewan[i] = new Hamsters(detail[0], Integer.parseInt(detail[1]));
					}
					else if(species.equals("parrot")){ 
						infoMasingHewan[i] = new Parrots(detail[0], Integer.parseInt(detail[1]));
					}
					else if(species.equals("lion")){ 
						infoMasingHewan[i] = new Lions(detail[0], Integer.parseInt(detail[1]));
					}
			}
			return infoMasingHewan;
		}
	}
	
	public static void visit(Animals animal, String name, String species){
		if(species.equals("cat")){
			((Cats)animal).visit(name);
		}
		else if(species.equals("eagle")){
			((Eagles)animal).visit(name);
		}
		else if(species.equals("hamster")){
			((Hamsters)animal).visit(name);
		}
		else if(species.equals("parrot")){
			((Parrots)animal).visit(name);
		}
		else{
			((Lions)animal).visit(name);
		}
		System.out.println("Back to office!");
	}	
}