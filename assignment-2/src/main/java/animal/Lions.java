package animal;

import java.util.Scanner;

public class Lions extends Animals{
	
	public Lions(String name, int length){
		super(name, length, "wild");
	}
	
	public void hunt(){
		System.out.println(this.name + " makes a voice: Err....");
	}
	
	public void brushMane(){
		System.out.println(this.name + " makes a voice: Hauhhmm!");
	}
	
	public void disturbed(){
		System.out.println(this.name + " makes a voice: HAUHHMM!");
	}
	
	public void visit(String name){
		Scanner input = new Scanner(System.in);
		System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
		String command = input.nextLine();
		if(command.equals("1")){
			this.hunt();
		}
		else if(command.equals("2")){
			this.brushMane();
		}
		else if(command.equals("3")){
			this.disturbed();
		}
		else{
			System.out.println("You do nothing...");
		}
	}
}