import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;


public class Board extends JFrame{

    private static int count = 0;
    private JLabel countLabel = new JLabel("Number of attempt: 0");
    private ImageIcon coverIcon;
    private List<Card> cards;
    private Card selectedCard;
    private JPanel countPanel;
    private JPanel panelMain;
    private JPanel control;
    private Card c;
    private Card c1;
    private Card c2;
    private Timer t;
    private List<BufferedImage> icons = new ArrayList<>();
    private JButton resetButton = new JButton("Play again?");
    private JButton exitButton = new JButton("Exit");

    public Board(){

		File dir = new File("C:\\Users\\Yudha Pradipta R\\Documents\\DDP 2\\TP\\TPDDP2\\assignment-4\\img\\Tp4\\");
		for (File f : dir.listFiles()){
			BufferedImage icon = null;
			try{
				icon = ImageIO.read(f);
				icons.add(icon);
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		
		
		int pairs = 18;
        List<Card> cardsList = new ArrayList<Card>();
		
		for(int i=0;i<pairs;i++){
			cardsList.add(new Card(i, icons.get(i)));
            cardsList.add(new Card(i, icons.get(i)));
		}
		Collections.shuffle(cardsList);
		this.cards = cardsList;
																

        //set up the timer
        t = new Timer(750, new ActionListener(){
            public void actionPerformed(ActionEvent ae){
                checkCards();
            }
        });

        t.setRepeats(false);
		
		BufferedImage img = null;
        try{
            img = ImageIO.read(new File("C:\\Users\\Yudha Pradipta R\\Documents\\DDP 2\\TP\\TPDDP2\\assignment-4\\img\\cover.png"));

        }catch (Exception a){
            a.printStackTrace();
        }

        coverIcon = new ImageIcon(img);

        //set up the board itself
        Container pane = getContentPane();
		panelMain = new JPanel();
        panelMain.setLayout(new GridLayout(6, 6));
        for (Card c : cards){
            c.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    selectedCard = c;
                    doTurn();
                }
            });
            c.setIcon(coverIcon);
            panelMain.add(c);
        }
	
	    control = new JPanel(new FlowLayout());
        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        control.add(resetButton);
        control.add(exitButton);

        countPanel = new JPanel(new FlowLayout());
        countPanel.add(countLabel);

        pane.add(panelMain, BorderLayout.NORTH);
        pane.add(control, BorderLayout.CENTER);
        pane.add(countPanel, BorderLayout.SOUTH);
        setTitle("Pair the Animal");
    }

    public void doTurn(){
        if (c1 == null && c2 == null){
            c1 = selectedCard;
            c1.setIcon(c1.getGambar());
        }

        if (c1 != null && c1 != selectedCard && c2 == null){
            c2 = selectedCard;
            c2.setIcon(c2.getGambar());
            t.start();

        }
    }

    public void checkCards(){
        if (c1.getId() == c2.getId()){//match condition
            c1.setEnabled(false); //disables the button
            c2.setEnabled(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()){
                JOptionPane.showMessageDialog(this, "You win!");
                System.exit(0);
            }
        }

        else{
            c1.setIcon(coverIcon); //'hides' text
            c2.setIcon(coverIcon);
        }
        c1 = null; //reset c1 and c2
        c2 = null;
		count += 1;
        countLabel.setText("Number of attempt: " + getCount());
    }

    public boolean isGameWon(){
        for(Card c: this.cards){
            if (c.getMatched() == false){
                return false;
            }
        }
        return true;
    }
	
	    public void reset() {
            for (Card card : this.cards) {
                card.setMatched(false);
                card.setEnabled(true);
                card.setIcon(coverIcon);
            }
            count = 0;
            countLabel.setText("Number of attempt: " + getCount());

            Collections.shuffle(cards);
            panelMain.removeAll();
            for (Card card : cards) {
                panelMain.add(card);
        }
    }

    public static int getCount() {
        return count;
    }

}