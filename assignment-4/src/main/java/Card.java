import javax.swing.*;
import java.awt.image.BufferedImage;


public class Card extends JButton{
    private int id;
    private boolean matched = false;
	private ImageIcon gambar;


    public Card(int id, BufferedImage image){
        this.id = id;
		this.gambar = new ImageIcon(image);
    }

    public int getId(){
        return this.id;
    }


    public void setMatched(boolean matched){
        this.matched = matched;
    }

    public boolean getMatched(){
        return this.matched;
    }
	
	public ImageIcon getGambar(){
		return gambar;
	}
}