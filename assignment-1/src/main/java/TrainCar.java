public class TrainCar {
/**
*class ini digunakan untuk mendefinisikan TrainCar
*di class ini dihitung TotalWeight dan TotalMassIndex dalam rekursif
*@author Yudha Pradipta Ramadan
*/


    public static final double EMPTY_WEIGHT = 20; // In kilograms

    private WildCat cat;
	private TrainCar next;// TODO Complete me!

    public TrainCar(WildCat cat) {
		this.cat = cat;
        // TODO Complete me!
    }

    public TrainCar(WildCat cat, TrainCar next) {
		this.cat = cat;
		this.next = next;
        // TODO Complete me!
    }

    public double computeTotalWeight() {
        double totalWeight = 0;
		if(this.next != null){
			totalWeight = EMPTY_WEIGHT + this.cat.weight + next.computeTotalWeight();
		}
		else {
			totalWeight = EMPTY_WEIGHT + this.cat.weight;
		}
		// TODO Complete me!
        return totalWeight;
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
		double totalIndex = 0;
		if(this.next != null){
			totalIndex = this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
		}else{
			totalIndex = this.cat.computeMassIndex();
		}
        return totalIndex;
    }

    public void printCar() {
        // TODO Complete me!
		if(this.next != null){
			System.out.printf(String.format("(%s)--", this.cat.name));
            this.next.printCar();
        }else{
            System.out.printf(String.format("(%s)\n", this.cat.name));
        }
    }
}
