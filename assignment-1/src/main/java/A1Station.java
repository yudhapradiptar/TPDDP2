import java.util.Scanner;
public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    private static TrainCar LastTrain = null;
	// You can add new variables or methods in this class
	
	private static int TrainCount = 0;

    public static void main(String[] args) {
        // TODO Complete me!
		Scanner input = new Scanner(System.in);
		int jumlahKucing = Integer.parseInt(input.nextLine());
		for(int a = 0;a < jumlahKucing;a++){
			String[] inputKucing = input.nextLine().split(",");
			String namaKucing = inputKucing[0];
			double beratKucing = Double.parseDouble(inputKucing[1]);
			double panjangKucing = Double.parseDouble(inputKucing[2]);
			WildCat kucing = new WildCat(namaKucing, beratKucing, panjangKucing);
			TrainCar kereta = new TrainCar(kucing, LastTrain);
			LastTrain = kereta;
			TrainCount++;
			double averageMI = LastTrain.computeTotalMassIndex()/TrainCount;
			if(LastTrain.computeTotalWeight() >= THRESHOLD || a == jumlahKucing-1){
				System.out.println("The train departs to Javari Park");
				System.out.print("[LOCO]<--");
				LastTrain.printCar();
				System.out.println("Average mass index of all cats: " + String.format("%.2f", averageMI));
				LastTrain = null;
				TrainCount = 0;
				if (averageMI < 18.5){
					System.out.println("In average, the cats in the train are *underweight*");
				}
				else if(averageMI >= 18.5 && averageMI < 25){
					System.out.println("In average, the cats in the train are *normal*");
				}
				else if(averageMI >= 25 && averageMI < 30){
					System.out.println("In average, the cats in the train are *overweight*");
				}
				else if(averageMI >= 30){
					System.out.println("In average, the cats in the train are *obese*");
				}
			}
		}

    }
}
