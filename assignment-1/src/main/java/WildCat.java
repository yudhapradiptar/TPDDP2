public class WildCat {
/**
*class ini digunakan untuk mendefinisikan WildCat seperti nama, berat dan panjang
*computeMassIndex untuk menghitung BMI WildCat dan mereturn BMI
*@author Yudha Pradipta Ramadan
*/

    // TODO Complete me!
    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
       super(); // TODO Complete me!
	   this.name = name;
	   this.weight = weight;
	   this.length = length;
    }

    public double computeMassIndex() {
        double BMI = weight/Math.pow(length/100,2);// TODO Complete me!
        return BMI;
    }
}
